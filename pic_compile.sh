#!/bin/bash
# Compile screenshots in the working directory into a single image
# https://gitlab.com/LostPon/scripts

# Images are cropped to the dimensions of Discord conversations
# File names should start with 'screenshot' and be ordered chronologically
# This is best used as a function in .bash_functions

# Crop screenshots
for IMAGE in screenshot*; do
    convert $IMAGE -crop 1014x514+334+165 "${IMAGE/screenshot/cropped}";
done

# use the directory's name
OUTPUT_NAME="${PWD/*\//}.jpg";

# https://legacy.imagemagick.org/Usage/montage/#geometry_size
montage cropped* -tile 1x6 -geometry 1014x514 miff:- | convert -  +append "$OUTPUT_NAME"; # stack 6 pictures vertically before creating a new column

rm cropped*;
echo; echo "compiled into: $OUTPUT_NAME";
