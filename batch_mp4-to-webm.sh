#!/bin/bash
# Converts all the mp4 files in the working directory to webm under 4Mio
# https://gitlab.com/LostPon/scripts

F_MAXIZE=4096 # Set max file size
A_BR=15 # Set audio bitrate

echo "maxize: "$F_MAXIZE;
echo "a_br: "$A_BR;

for F_NAME in *.mp4; do
    
    # Replace white spaces by underscores
    F_NAME=${F_NAME// /_};
    
    # Control file size
    F_SIZE=`du -k "$F_NAME" | cut -f1`;
    F_SIZE=$(($F_SIZE + 100));
    
    # Extract video duration
    F_DUR=$(ffprobe -hide_banner -v quiet -show_format -print_format json -of default=noprint_wrappers=1 $F_NAME | grep "duration" | sed 's/^[a-Z]*=//' | sed 's/\.[0-9]*//') ;
    F_DUR=$(($F_DUR + 1));

    # Set video bitrate by dividing maxsize by vid duration
    if [$F_SIZE -gt $F_MAXIZE]
    then
	V_BR=$(($F_MAXIZE / $F_DUR));
	echo "V_BR= "$F_MAXIZE" / "$F_DUR;
    else
	V_BR=$(($F_SIZE / $F_DUR));
	echo "V_BR= "$F_SIZE" / "$F_DUR;
    fi
    
    # Substract audio bitrate, and append "KB" to the variable
    echo "V_BR= "$V_BR" - "$A_BR;
    V_BR=$(($V_BR - $A_BR))"KB";
    echo "V_BR: "$V_BR; echo;
    
    # Create webm
    RESULT_NAME="${F_NAME%.*}.webm";
    ffmpeg -i $F_NAME -c:v libvpx -b:v $V_BR -c:a libvorbis -crf 6 $RESULT_NAME
    echo "output file name: "$RESULT_NAME; echo "-------------"; echo; echo;

    if [ -e ../mp4/ ]; then mv $F_NAME ../mp4/; fi
    if [ -e ../webm/ ]; then mv $RESULT_NAME ../webm/; fi 
done
